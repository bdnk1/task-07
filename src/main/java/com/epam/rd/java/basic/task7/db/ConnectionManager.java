package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

        private static final String URL = "connection.url";

        static {
            loadDriver();
        }

        private ConnectionManager() {
        }

        public static Connection open() {
            try {
                return DriverManager.getConnection(
                        PropertiesUtil.get(URL)
                );
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        private static void loadDriver() {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

