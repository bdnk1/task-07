package com.epam.rd.java.basic.task7.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class PropertiesUtil {
    private static final Properties PROPERTIES = new Properties();

    static {
        loadProperties();
    }

    private PropertiesUtil() {
    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

    private static void loadProperties() {
        //java.io.File f = new File("./app.properties");
        //System.out.println(f.getAbsolutePath());
        try (java.io.InputStream inputStream = new FileInputStream("./app.properties")) {
            PROPERTIES.load(inputStream);
        } catch ( IOException e) {
            throw new RuntimeException(e);
        }
    }
}
