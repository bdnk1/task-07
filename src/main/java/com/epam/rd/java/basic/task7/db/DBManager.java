package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
            return instance;
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        String query = "SELECT * FROM USERS ";
        List<User> allUsersList = new ArrayList<>();
        try (Connection con = ConnectionManager.open();
             Statement stmt = con.createStatement()
        ) {
            ResultSet res = stmt.executeQuery(query);
            while (res.next()) {
                allUsersList.add(getUser(res.getString("login")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return allUsersList;
    }

    public boolean insertUser(User user) throws DBException {
        String query = "INSERT INTO USERS(login) VALUES(?)";
        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query)
        ) {
            stmt.setString(1, user.getLogin());
            int count = stmt.executeUpdate();
            user.setId(getUser(user.getLogin()).getId());
            return count > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        String query = "DELETE FROM users ";

        try (Connection con = ConnectionManager.open();
             Statement stmt = con.createStatement();
        ) {

            String where = Arrays.stream(users)
                    .map(u -> "'" + u.getLogin() + "'")
                    .collect(Collectors.joining(", ", "WHERE login IN (", ")"));
            return stmt.executeUpdate(query + where) > 0;
        } catch (SQLException e) {
            throw new DBException("message", e);
        }
    }

    public User getUser(String login) throws DBException {
        String query = "SELECT * FROM users WHERE login = ?";
        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query)
        ) {
            stmt.setString(1, login);
            ResultSet resSet = stmt.executeQuery();
            if (resSet.next()) {
                User user = User.createUser(resSet.getString("login"));
                user.setId(resSet.getInt("id"));
                return user;
            }
        } catch (SQLException e) {
            throw new DBException("message", e);
        }

        return null;
    }

    public Team getTeam(String name) throws DBException {
        String query = "SELECT * FROM teams WHERE name = ?";
        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query)
        ) {
            stmt.setString(1, name);
            ResultSet resSet = stmt.executeQuery();
            if (resSet.next()) {
                Team team = Team.createTeam(resSet.getString("name"));
                team.setId(resSet.getInt("id"));
                return team;
            }
        } catch (SQLException e) {
            throw new DBException("message", e);
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        String query = "SELECT * FROM teams";
        List<Team> allTeamsList = new ArrayList<>();
        try (Connection con = ConnectionManager.open();
             Statement stmt = con.createStatement()
        ) {
            ResultSet res = stmt.executeQuery(query);
            while (res.next()) {
                allTeamsList.add(getTeam(res.getString("name")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return allTeamsList;
    }

    public boolean insertTeam(Team team) throws DBException {
        String query = "INSERT INTO teams (name) VALUES (?) ";
        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query)
        ) {
            stmt.setString(1, team.getName());
            int count = stmt.executeUpdate();
            team.setId(getTeam(team.getName()).getId());
            return count > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String query = "INSERT INTO users_teams (user_id,team_id) VALUES (?,?)";

        int inserted = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionManager.open();
            stmt = con.prepareStatement(query);
            con.setAutoCommit(false);
            for (Team team : teams) {
                stmt.setInt(1, user.getId());
                stmt.setInt(2, team.getId());
                inserted += stmt.executeUpdate();
            }
            con.commit();
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                throw new DBException("mee", e);
            }
            throw new DBException("mee", e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    throw new DBException("", e);
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    throw new DBException("", e);
                }
            }

        }
        return inserted > 0;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String query = "SELECT t.id,t.name FROM users_teams JOIN teams AS t ON team_id = t.id WHERE user_id = ?";
        List<Team> teams = new ArrayList<>();
        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query)
        ) {
            stmt.setInt(1, user.getId());
            ResultSet res = stmt.executeQuery();
            while (res.next()) {
                teams.add(getTeam(res.getString("name")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String query = " DELETE FROM teams WHERE name = ?";

        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query);
        ) {
            stmt.setString(1, team.getName());
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException("message", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        String query = "UPDATE teams SET name = ? WHERE id = ?";

        try (Connection con = ConnectionManager.open();
             PreparedStatement stmt = con.prepareStatement(query);
        ) {
            stmt.setString(1, team.getName());
            stmt.setInt(2, team.getId());
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException("message", e);
        }
    }

}
