package com.epam.rd.java.basic.task7;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

public class Main {
    public static void main(String[] args) throws DBException {
        DBManager dm = DBManager.getInstance();
        User user = User.createUser("testUser");
        User user2 = User.createUser("testUser");
        System.out.println(  user.getLogin());
        System.out.println(user.equals(user2));


        Team team = Team.createTeam("popa");
        Team team2 = Team.createTeam("popa2");
        Team team3 = Team.createTeam("popa3");

        System.out.println(team.getName());
        System.out.println(team.equals(team2));
        dm.insertUser(user);
        System.out.println("fdsf = "+user.getId());
        dm.insertTeam(team);
        dm.insertTeam(team2);
        dm.insertTeam(team3);
        dm.setTeamsForUser(dm.getUser("testUser"),dm.getTeam("popa"),dm.getTeam("popa2"),dm.getTeam("popa3"));
        System.out.println(dm.getUserTeams(dm.getUser("testUser")));

    }
}
